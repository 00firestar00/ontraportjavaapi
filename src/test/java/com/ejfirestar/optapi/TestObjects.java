package com.ejfirestar.optapi;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.junit.Test;

import java.util.ArrayList;

//TODO: Turn these into real test cases
public class TestObjects {
    private static final String API_ID = "2_22634_vlGOugwS8";
    private static final String API_KEY = "cxjQANHbskcLjv8";
    OntraportAPI opt_api = new OntraportAPI(API_ID, API_KEY);

    @Test
    public void testGetOne() {
        System.out.println(opt_api.object(0).getOne(100).exec());
    }

    @Test
    public void testGet() {
        System.out.println(opt_api.objects(0).get().exec());
    }

    @Test
    public void testPost() {
        String post_contact = opt_api.objects(0)
                .post()
                .addParameter("firstname", "JavaAPI")
                .addParameter("lastname", "TestAPI")
                .addParameter("email", "javaapi@ontraport.com")
                .exec();
        JsonObject json_contact = (JsonObject) new JsonParser().parse(post_contact);
        int new_id = json_contact.get("data").getAsJsonObject().get("id").getAsInt();
    }

    @Test
    public void testPut() {
        System.out.println(opt_api.objects(0).put(100)
                .addParameter("lastname", "NewLastName")
                .exec());
    }

    @Test
    public void testGetMeta() {
        System.out.println(opt_api.objects(0).getMeta("byId").exec());
    }

    @Test
    public void testPutTag() {
        // create new Tag
        String post_tag = opt_api.objects(14).post()
                .addParameter("tag_name", "JavaAPI")
                .exec();

        System.out.println(post_tag);

        JsonObject json_tag = (JsonObject) new JsonParser().parse(post_tag);
        int tag_id = json_tag.get("data").getAsJsonObject().get("tag_id").getAsInt();

        // add tags to contact
        ArrayList<Integer> add_list = new ArrayList<>();
        add_list.add(tag_id);
        add_list.add(3);

        ArrayList<Integer> ids = new ArrayList<>();
        ids.add(100);

        System.out.println(opt_api.objects(0).putTag(add_list, ids).exec());
    }

    @Test
    public void testDeleteTag() {

    }
}

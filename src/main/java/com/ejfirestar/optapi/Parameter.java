package com.ejfirestar.optapi;

public class Parameter {

    public String key;
    public String value;

    public Parameter(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public Parameter(String key, int value) {
        this.key = key;
        this.value = Integer.toString(value);
    }

    @Override
    public String toString() {
        return "&" + key + "=" + value;
    }
}

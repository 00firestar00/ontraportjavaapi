package com.ejfirestar.optapi;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class OntraportObject {

    private final int object_type_id;
    private Request.Builder builder;
    private OkHttpClient client = new OkHttpClient();
    private ArrayList<Parameter> parameters = new ArrayList<>();

    public OntraportObject(int object_type_id, Request.Builder builder) {
        this.object_type_id = object_type_id;
        this.builder = builder;
        parameters.add(new Parameter("objectID", object_type_id));
    }

    public OntraportObject getOne(int id) {
        String params = String.format("object?objectID=%d&id=%d", object_type_id, id);
        builder.url(OntraportAPI.ENDPOINT + params);
        return this;
    }

    public OntraportObject get() {
        StringBuilder sb = new StringBuilder();
        sb.append(OntraportAPI.ENDPOINT).append("objects?");
        for (Parameter parameter : parameters) {
            sb.append(parameter.toString());
        }
        builder.url(sb.toString());
        return this;
    }

    public OntraportObject put(int id) {
        FormEncodingBuilder form_builder = new FormEncodingBuilder();
        form_builder.add("id", Integer.toString(id));
        for (Parameter parameter : parameters) {
            form_builder.add(parameter.key, parameter.value);
        }
        builder.url(OntraportAPI.ENDPOINT + "objects?")
                .post(form_builder.build());

        return this;
    }

    public OntraportObject post() {
        FormEncodingBuilder form_builder = new FormEncodingBuilder();
        for (Parameter parameter : parameters) {
            form_builder.add(parameter.key, parameter.value);
        }
        builder.url(OntraportAPI.ENDPOINT + "objects?")
                .post(form_builder.build());

        return this;
    }

    public OntraportObject getMeta(String format) {
        String params = String.format("objects/meta?objectID=%d&format=%s", object_type_id, format);
        builder.url(OntraportAPI.ENDPOINT + params);
        return this;
    }

    public OntraportObject putTag(List<Integer> add_list, List<Integer> ids) {
        String tag_list = "";
        for (Integer i : add_list) {
            tag_list += i.toString() + ',';
        }
        String id_list = "";
        for (Integer i : ids) {
            id_list += i.toString() + ',';
        }

        RequestBody form = new FormEncodingBuilder()
                .add("add_list", tag_list)
                .add("ids", id_list)
                .build();

        builder.url(OntraportAPI.ENDPOINT + "objects/tag?")
                .post(form);
        return this;
    }

    public OntraportObject deleteTag(List<Integer> remove_list, List<Integer> ids) {
        String tag_list = "";
        for (Integer i : remove_list) {
            tag_list += i.toString() + ',';
        }
        String id_list = "";
        for (Integer i : ids) {
            id_list += i.toString() + ',';
        }

        RequestBody form = new FormEncodingBuilder()
                .add("remove_list", tag_list)
                .add("ids", id_list)
                .build();

        builder.url(OntraportAPI.ENDPOINT + "objects/tag?")
                .post(form);
        return this;
    }

    public OntraportObject addParameter(String key, String value) {
        return this;
    }

    public String exec() {
        try {
            Response response = client.newCall(builder.build()).execute();
            return response.body().string();
        }
        catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

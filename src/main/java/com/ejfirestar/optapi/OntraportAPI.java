package com.ejfirestar.optapi;

import com.squareup.okhttp.Request;

public class OntraportAPI {

    public static final String ENDPOINT = "https://api.ontraport.com/1/";

    private Request.Builder builder;
    private String api_id;
    private String api_key;

    public OntraportAPI(String api_id, String api_key) {
        this.api_id = api_id;
        this.api_key = api_key;
        builder = new Request.Builder()
                .header("Api-Appid", api_id)
                .header("Api-Key", api_key);
    }

    public String getApiId() {
        return api_id;
    }

    public String getApiKey() {
        return api_key;
    }

    public OntraportObject object(int object_type_id) {
        return new OntraportObject(object_type_id, builder);
    }

    public OntraportObject objects(int object_type_id) {
        return new OntraportObject(object_type_id, builder);
    }
}

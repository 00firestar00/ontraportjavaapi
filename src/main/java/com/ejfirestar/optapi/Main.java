package com.ejfirestar.optapi;


import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Main {

    public static void main(String... args) {
        if (args.length < 2) {
            System.out.println("API id and key are required");
        }
        OntraportAPI opt_api = new OntraportAPI(args[0], args[1]);
        String notes = opt_api.objects(12).get().exec();
        JsonParser parser = new JsonParser();
        JsonObject json_notes = (JsonObject) parser.parse(notes);
        if (json_notes.get("code").getAsInt() == 0) {
            JsonArray data = json_notes.get("data").getAsJsonArray();
            System.out.println("Contact ID, First Name, Last Name, Email, Note");
            for (JsonElement e : data) {
                String contact_id = e.getAsJsonObject().get("contact_id").getAsString();
                String note_data = e.getAsJsonObject().get("data").getAsString();

                String contact = opt_api.objects(0)
                        .getOne(Integer.parseInt(contact_id))
                        .addParameter("listFields", "id,firstname,lastname,email")
                        .exec();

                JsonObject json_contact = (JsonObject) parser.parse(contact);
                if (json_notes.get("code").getAsInt() == 0) {
                    JsonObject contact_data = json_contact.get("data").getAsJsonObject();
                    String firstname = contact_data.get("firstname").getAsString();
                    String lastname = contact_data.get("lastname").getAsString();
                    String email = contact_data.get("email").getAsString();

                    System.out.println(contact_id + ',' + firstname + ',' + lastname + ',' + email + ',' + note_data);
                }
            }
        }
        else {
            System.out.println("Request failed");
        }
    }
}
